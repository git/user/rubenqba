# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit eutils gnome2

DESCRIPTION="An RSS reader plugin for Evolution"
HOMEPAGE="http://www.dropbox.com"
SRC_URI="https://www.dropbox.com/download?dl=packages/${PN}-${PV}.tar.bz2
	x86? ( http://dl-web.dropbox.com/u/17/dropbox-lnx.x86-0.7.110.tar.gz )
	amd64? ( http://dl-web.dropbox.com/u/17/dropbox-lnx.x86_64-0.7.110.tar.gz )"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"


RDEPEND="x11-libs/gtk+:2
	x11-libs/libnotify
	gnome-base/nautilus
	dev-libs/dbus-glib
	x11-libs/libSM"
DEPEND="${RDEPEND}"

src_compile() {
	gnome2_src_prepare
}

pkg_setup() {
	arch=${ARCH}
	use amd64 && arch="x86_64"	
	einfo "D=${D};"
	einfo "S=${S};"
	einfo "W=${WORKDIR};"
	einfo "P=${P};"
}

src_unpack() {
	unpack dropbox-lnx.${arch}-0.7.110.tar.gz
	mv ${WORKDIR}/.dropbox-dist ${WORKDIR}/dropbox
	unpack ${PN}-${PV}.tar.bz2
}

src_install() {	
	gnome2_src_install
	
	rm ${D}/usr/bin/dropbox
	insinto /opt
	doins -r ../dropbox || die
	chmod +x ${D}/opt/dropbox/dropboxd
	chmod +x ${D}/opt/dropbox/dropbox
	dosym /opt/dropbox/dropboxd /usr/bin
	dosym /opt/dropbox/dropbox /usr/bin
}

