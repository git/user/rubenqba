# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

GCONF_DEBUG="no"

inherit autotools eutils gnome2 multilib

DESCRIPTION="Gobject-Introspection file Repository"
HOMEPAGE="http://live.gnome.org/GObjectIntrospection/"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="avahi babl dbus gconf gnome-keyring goocanvas gtksourceview libnotify libwnck nautilus poppler vte"

RDEPEND=">=dev-libs/gobject-introspection-0.6.5"
DEPEND="${RDEPEND}
	avahi? ( >=net-dns/avahi-0.6 )
	babl? ( media-libs/babl )
	dbus? ( dev-libs/dbus-glib )
	gconf? ( gnome-base/gconf )
	gnome-keyring? ( gnome-base/gnome-keyring )
	goocanvas? ( x11-libs/goocanvas )
	gtksourceview? ( x11-libs/gtksourceview )
	libnotify? ( x11-libs/libnotify )
	libsoup? ( net-libs/libsoup:2.4 )
	libwnck? ( x11-libs/libwnck )
	nautilus? ( gnome-base/nautilus )
	poppler? ( >=app-text/poppler-0.8[cairo] )
	vte? ( x11-libs/vte )"

pkg_setup() {	
	skipped="Gst,Unique,GMenu,GSSDP,WebKit"
	if use !gconf; then
		skipped="${skipped},GConf"
	fi;
	if use !gnome-keyring; then
		skipped="${skipped},GnomeKeyring"
	fi;
	if use !goocanvas; then
		skipped="${skipped},GooCanvas"
	fi;
	if use !libnotify; then
		skipped="${skipped},Notify"
	fi;
	if use !libwnck; then
		skipped="${skipped},Wnck"
	fi;
	if use !poppler; then
		skipped="${skipped},Poppler"
	fi;
	if use !nautilus; then
		skipped="${skipped},Nautilus"
	fi;
	G2CONF="${G2CONF} --with-skipped-gir-modules=${skipped}"
}

src_prepare() {
	gnome2_src_prepare
	eautoreconf
}

src_install() {
	gnome2_src_install

	# These are needed to build other .girs and .typelibs,
	# but they shouldn't be installed since they are provided by gtk+/atk/pango
	for i in Pango{,FT2,Cairo,Xft,X}-1.0 Atk-1.0 Gtk-2.0 Gdk{,Pixbuf}-2.0; do
		rm -f "${D}/usr/$(get_libdir)/girepository-1.0/${i}.typelib"
		rm -f "${D}/usr/share/gir-1.0/${i}.gir"
	done
	rm -f ${D}/usr/$(get_libdir)/*{Gdk,Gtk}*
}

