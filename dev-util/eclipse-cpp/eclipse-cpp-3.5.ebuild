# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: ruben$

inherit eutils

DESCRIPTION="Eclipse IDE for C/C++"
HOMEPAGE="http://www.eclipse.org/"
SRC_URI="x86? ( http://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/galileo/R/${PN}-galileo-linux-gtk.tar.gz )
    amd64? ( http://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/galileo/R/${PN}-galileo-linux-gtk-x86_64.tar.gz )
    cmake? ( http://downloads.sourceforge.net/project/cmakeed/CMakeEd%201.1/CMakeEd%201.1.2/CMakeEd-1.1.2.zip )
    subversion? ( http://subclipse.tigris.org/files/documents/906/45156/site-1.4.8.zip )"

SLOT="3.5"
LICENSE="EPL-1.0"
IUSE="cmake subversion git"
KEYWORDS="~amd64 ~x86"

S=${WORKDIR}

RDEPEND=">=virtual/jre-1.5
	cmake? ( dev-util/cmake )
	subversion? ( dev-util/subversion )
	git? ( dev-vcs/git )"

DEPEND=">=virtual/jdk-1.5
        sys-apps/findutils
        app-arch/unzip
        app-arch/zip"

pkg_setup() {
	arch=${ARCH}
        use amd64 && arch="-x86_64"
}

src_unpack() {
	einfo "Trabajando en directorio ${S}"
	unpack ${PN}-galileo-linux-gtk${arch}.tar.gz

	# No warnings / Java 5 / all output should be directed to stdout
	#find "${S}" -type f -name '*.xml' -exec \
	#	sed -r -e "s:(-encoding ISO-8859-1):\1 -nowarn:g" -e "s:(\"compilerArg\" value=\"):\1-nowarn :g" \
	#	-e "s:(<property name=\"javacSource\" value=)\".*\":\1\"1.5\":g" \
	#	-e "s:(<property name=\"javacTarget\" value=)\".*\":\1\"1.5\":g" -e "s:output=\".*(txt|log).*\"::g" -i {} \;

	#JDK home
	while read line; do
		java-ant_rewrite-classpath "$line" > /dev/null
	done < <(find "${S}" -type f -name "build.xml" )
	
	mv "eclipse" "${PN}"
	
	#unpack plugins
	if use cmake; then	
		unpack CMakeEd-1.1.2.zip
		mv "site.xml" "site-cmakeed.xml"
	fi;
	
	if use subversion; then
		unpack site-1.4.8.zip
		mv "site.xml" "site-subeclipse.xml"
	fi;
	
	if use git; then
		unzip "${FILESDIR}/egit-0.4.0.200904091634.zip"
	fi;
}

src_install() {
	dodir /usr/lib

	einfo "D=${D};"
	
	insinto /usr/lib
	doins -r ${PN} || die

	#Chmod the actual binary in the Eclipse dir
	chmod +x "${D}/usr/lib/${PN}/eclipse" || die

	# Install startup script
	newbin "${FILESDIR}"/eclipse-galileo ${P} || die
	
	insinto /usr/lib/${PN}
	doins "${FILESDIR}/eclipse-cdt.png"

	make_desktop_entry "${P}" "Eclipse ${PV} (Galileo)" "/usr/lib/${PN}/eclipse-cdt.png" "Development" ||  die
	
	einfo "Install plugins"
	[ -d plugins ] && (doins -r plugins  || die)
	[ -d features ] && (doins -r features || die)
	[ -f *.xml ] && (doins *.xml || die)
}

pkg_postinst() {
	einfo
	einfo "Welcome to Eclipse-3.5 (Galileo)!"
	einfo
	einfo "You can now install plugins via Update Manager without any"
	einfo "tweaking. This is the recommended way to install new features for Eclipse."
	einfo
	einfo "Please read http://gentoo-wiki.com/Eclipse"
	einfo "It contains a lot of useful information and help about Eclipse on Gentoo."
}

