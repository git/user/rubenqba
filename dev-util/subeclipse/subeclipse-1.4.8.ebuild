# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Eclipse plugin for Subversion"
HOMEPAGE="http://cmakeed.sourceforge.net"
SRC_URI="http://downloads.sourceforge.net/project/cmakeed/CMakeEd%201.1/CMakeEd%201.1.2/CMakeEd-1.1.2.zip"

SLOT="1.4.8"
LICENSE="EPL-1.0"
IUSE=""
KEYWORDS="~amd64 ~x86"

ECLIPSE_DIR="/usr/lib/eclipse-cpp"

RDEPEND=">=dev-util/eclipse-cpp-3.5"

DEPEND=">=dev-util/eclipse-cpp-3.5
	app-arch/unzip
        app-arch/zip"

src_unpack() {
	einfo "Trabajando en directorio ${S}"
	unpack ${A}
	mv "site.xml" "cmakeed-site.xml"	
}

src_install() {
	dodir /usr/lib/eclipse-cpp

	insinto /usr/lib/eclipse-cpp
	doins -r * || die
}

pkg_postinst() {
	einfo
	einfo "Restart Eclipse-3.5"
	einfo
}

