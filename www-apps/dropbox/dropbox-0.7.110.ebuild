# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit eutils

DESCRIPTION="An RSS reader plugin for Evolution"
HOMEPAGE="http://www.dropbox.com"
SRC_URI="x86? ( http://dl-web.dropbox.com/u/17/${PN}-lnx.x86-${PV}.tar.gz )
    amd64? ( http://dl-web.dropbox.com/u/17/${PN}-lnx.x86_64-${PV}.tar.gz )"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"


RDEPEND="dev-libs/dbus-glib
	x11-libs/gtk+:2
	x11-libs/libSM"
DEPEND="${RDEPEND}"

pkg_setup() {
	arch=${ARCH}
	use amd64 && arch="x86_64"	
	einfo "D=${D};"
	einfo "S=${S};"
	einfo "W=${WORKDIR};"
	einfo "P=${P};"
}

src_unpack() {
	unpack ${PN}-lnx.x86_64-${PV}.tar.gz
	mv ${WORKDIR}/.dropbox-dist ${S}	
}

src_install() {
	insinto /opt/dropbox
	doins -r * || die
	dosym /opt/dropbox/dropboxd /usr/bin/dropboxd

#	make_desktop_entry "${P}" "Dropbox ${PV}" "/opt/${PN}/dropbox.png" "Internet" ||  die
	
}
